﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace LIQUIDADORDESUELDOS
{
    public partial class frmAltaRecibo : Form
    {
        public frmAltaRecibo()
        {
            InitializeComponent();
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "MM/yyyy";

            comboEmpleado.DataSource = null;
            comboEmpleado.DataSource = EMPLEADO.listarEmpleados();

            Enlazar();
        }

        void Enlazar()
        {
            foreach (CONCEPTO c in (from CONCEPTO c in CONCEPTO.listarConceptos()
                                    where c.Estado == 1
                                    select c).ToList())
            {
                checkBoxConceptos.Items.Add(c);
            }

            dtgRecibos.DataSource = null;
            dtgRecibos.DataSource = RECIBO.listarRecibos();
        }

        private void frmAltaRecibo_Load(object sender, EventArgs e)
        {

        }

        private void comboMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAltaRecibo_Click(object sender, EventArgs e)
        {
            List<RECIBO> recibos = RECIBO.listarRecibos();
            if(VALIDADOR.ValidaFloat(txtSueldoBruto.Text) && VALIDADOR.ValidaReciboExistente(((EMPLEADO)comboEmpleado.SelectedItem).Legajo, int.Parse(dateTimePicker1.Value.Month.ToString()), int.Parse(dateTimePicker1.Value.Year.ToString())))
            {
                List<CONCEPTO> conceptos = new List<CONCEPTO>();

                for (int i = 0; i < checkBoxConceptos.Items.Count; i++)
                {
                    if (checkBoxConceptos.GetItemChecked(i))
                    {
                        conceptos.Add((CONCEPTO)checkBoxConceptos.Items[i]);
                    }
                }

                lblEstado.Text = RECIBO.GenerarRecibo(((EMPLEADO)comboEmpleado.SelectedItem).Legajo, int.Parse(dateTimePicker1.Value.Month.ToString()), int.Parse(dateTimePicker1.Value.Year.ToString()), float.Parse(txtSueldoBruto.Text), conceptos);
                Enlazar();

                txtSueldoBruto.Clear();
            }
        }
    }
}

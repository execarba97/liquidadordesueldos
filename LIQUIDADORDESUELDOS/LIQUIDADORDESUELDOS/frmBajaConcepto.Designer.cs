﻿namespace LIQUIDADORDESUELDOS
{
    partial class frmBajaConcepto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtgConceptos = new System.Windows.Forms.DataGridView();
            this.lblResultado = new System.Windows.Forms.Label();
            this.btnBajaConcepto = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgConceptos)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgConceptos
            // 
            this.dtgConceptos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgConceptos.Location = new System.Drawing.Point(12, 12);
            this.dtgConceptos.Name = "dtgConceptos";
            this.dtgConceptos.RowHeadersWidth = 51;
            this.dtgConceptos.RowTemplate.Height = 24;
            this.dtgConceptos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgConceptos.Size = new System.Drawing.Size(453, 296);
            this.dtgConceptos.TabIndex = 23;
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.Location = new System.Drawing.Point(587, 12);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(105, 17);
            this.lblResultado.TabIndex = 22;
            this.lblResultado.Text = "Sin informacion";
            // 
            // btnBajaConcepto
            // 
            this.btnBajaConcepto.Location = new System.Drawing.Point(471, 12);
            this.btnBajaConcepto.Name = "btnBajaConcepto";
            this.btnBajaConcepto.Size = new System.Drawing.Size(110, 77);
            this.btnBajaConcepto.TabIndex = 21;
            this.btnBajaConcepto.Text = "Baja concepto";
            this.btnBajaConcepto.UseVisualStyleBackColor = true;
            this.btnBajaConcepto.Click += new System.EventHandler(this.btnBajaConcepto_Click);
            // 
            // frmBajaConcepto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dtgConceptos);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.btnBajaConcepto);
            this.Name = "frmBajaConcepto";
            this.Text = "frmBajaConcepto";
            ((System.ComponentModel.ISupportInitialize)(this.dtgConceptos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgConceptos;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Button btnBajaConcepto;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace LIQUIDADORDESUELDOS
{
    public partial class frmModificacionConcepto : Form
    {
        public frmModificacionConcepto()
        {
            InitializeComponent();
            Enlazar();
        }

        void Enlazar()
        {
            dtgConceptos.DataSource = null;
            dtgConceptos.DataSource = (from CONCEPTO c in CONCEPTO.listarConceptos()
                                       where c.Estado == 1
                                       select c).ToList();
        }

        private void btnModificarConcepto_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaFloat(txtPorcentaje.Text) && VALIDADOR.ValidaText(txtDescripcion.Text))
            {
                CONCEPTO cons = (CONCEPTO)dtgConceptos.SelectedRows[0].DataBoundItem;

                if (float.Parse(txtPorcentaje.Text) <= 100)
                {
                    enumConceptos en = new enumConceptos();
                    if (rbSuma.Checked)
                    {
                        en = enumConceptos.suma;
                    }
                    else
                    {
                        en = enumConceptos.resta;
                    }

                    lblResultado.Text = CONCEPTO.ModificaConcepto(cons.ID, txtDescripcion.Text, float.Parse(txtPorcentaje.Text) / 100, en);
                    Enlazar();
                }
                else
                {
                    MessageBox.Show("El porcentaje debe ser igual o inferior a 100.");
                }
            }
        }

        private void dtgConceptos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            CONCEPTO cons = (CONCEPTO)dtgConceptos.SelectedRows[0].DataBoundItem;

            txtDescripcion.Text = cons.Descripcion;

            if(cons.Porcentaje > 1)
            {
                txtPorcentaje.Text = ((cons.Porcentaje - 1) * 100).ToString();
                rbSuma.Checked = true;
            }
            else
            {
                txtPorcentaje.Text = ((cons.Porcentaje) * 100).ToString();
                rbResta.Checked = true;
            }
        }
    }
}

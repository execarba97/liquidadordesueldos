﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace LIQUIDADORDESUELDOS
{
    public partial class frmAltaConcepto : Form
    {
        public frmAltaConcepto()
        {
            InitializeComponent();
            Enlazar();
        }

        private void btnAgregarConcepto_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaText(txtDescripcion.Text) && VALIDADOR.ValidaFloat(txtPorcentaje.Text))
            {
                if (int.Parse(txtPorcentaje.Text) <= 100)
                {
                    enumConceptos en = new enumConceptos();
                    if (rbSuma.Checked)
                    {
                        en = enumConceptos.suma;
                    }
                    else
                    {
                        en = enumConceptos.resta;
                    }

                    lblResultado.Text = CONCEPTO.AltaConcepto(txtDescripcion.Text, float.Parse(txtPorcentaje.Text) / 100, en);
                    Enlazar();
                }
                else
                {
                    MessageBox.Show("El porcentaje debe ser igual o inferior a 100.");
                }
            }
        }

        void Enlazar()
        {
            dtgConceptos.DataSource = null;
            dtgConceptos.DataSource = (from CONCEPTO c in CONCEPTO.listarConceptos()
                                       where c.Estado == 1
                                       select c).ToList();
        }
    }
}

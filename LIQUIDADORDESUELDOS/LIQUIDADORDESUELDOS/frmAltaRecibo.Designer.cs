﻿namespace LIQUIDADORDESUELDOS
{
    partial class frmAltaRecibo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnAltaRecibo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblEstado = new System.Windows.Forms.Label();
            this.txtSueldoBruto = new System.Windows.Forms.TextBox();
            this.checkBoxConceptos = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtgRecibos = new System.Windows.Forms.DataGridView();
            this.comboEmpleado = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgRecibos)).BeginInit();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(131, 12);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(308, 22);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // btnAltaRecibo
            // 
            this.btnAltaRecibo.Location = new System.Drawing.Point(445, 40);
            this.btnAltaRecibo.Name = "btnAltaRecibo";
            this.btnAltaRecibo.Size = new System.Drawing.Size(80, 66);
            this.btnAltaRecibo.TabIndex = 3;
            this.btnAltaRecibo.Text = "Generar recibo";
            this.btnAltaRecibo.UseVisualStyleBackColor = true;
            this.btnAltaRecibo.Click += new System.EventHandler(this.btnAltaRecibo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Fecha del recibo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Sueldo bruto";
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Location = new System.Drawing.Point(531, 45);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(105, 17);
            this.lblEstado.TabIndex = 7;
            this.lblEstado.Text = "Sin informacion";
            // 
            // txtSueldoBruto
            // 
            this.txtSueldoBruto.Location = new System.Drawing.Point(131, 40);
            this.txtSueldoBruto.Name = "txtSueldoBruto";
            this.txtSueldoBruto.Size = new System.Drawing.Size(308, 22);
            this.txtSueldoBruto.TabIndex = 8;
            // 
            // checkBoxConceptos
            // 
            this.checkBoxConceptos.FormattingEnabled = true;
            this.checkBoxConceptos.Location = new System.Drawing.Point(131, 71);
            this.checkBoxConceptos.Name = "checkBoxConceptos";
            this.checkBoxConceptos.Size = new System.Drawing.Size(308, 89);
            this.checkBoxConceptos.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Fecha del recibo";
            // 
            // dtgRecibos
            // 
            this.dtgRecibos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgRecibos.Location = new System.Drawing.Point(15, 166);
            this.dtgRecibos.Name = "dtgRecibos";
            this.dtgRecibos.RowHeadersWidth = 51;
            this.dtgRecibos.RowTemplate.Height = 24;
            this.dtgRecibos.Size = new System.Drawing.Size(794, 223);
            this.dtgRecibos.TabIndex = 11;
            // 
            // comboEmpleado
            // 
            this.comboEmpleado.FormattingEnabled = true;
            this.comboEmpleado.Location = new System.Drawing.Point(519, 12);
            this.comboEmpleado.Name = "comboEmpleado";
            this.comboEmpleado.Size = new System.Drawing.Size(288, 24);
            this.comboEmpleado.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(442, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Empleado";
            // 
            // frmAltaRecibo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 607);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboEmpleado);
            this.Controls.Add(this.dtgRecibos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.checkBoxConceptos);
            this.Controls.Add(this.txtSueldoBruto);
            this.Controls.Add(this.lblEstado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAltaRecibo);
            this.Controls.Add(this.dateTimePicker1);
            this.Name = "frmAltaRecibo";
            this.Text = "frmAltaRecibo";
            this.Load += new System.EventHandler(this.frmAltaRecibo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgRecibos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button btnAltaRecibo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.TextBox txtSueldoBruto;
        private System.Windows.Forms.CheckedListBox checkBoxConceptos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dtgRecibos;
        private System.Windows.Forms.ComboBox comboEmpleado;
        private System.Windows.Forms.Label label4;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace LIQUIDADORDESUELDOS
{
    public partial class frmModificacionEmpleado : Form
    {
        public frmModificacionEmpleado()
        {
            InitializeComponent();
            Enlazar();
        }

        void Enlazar()
        {
            dtgEmpleados.DataSource = null;
            dtgEmpleados.DataSource = (from EMPLEADO e in EMPLEADO.listarEmpleados()
                                       where e.Estado == 1
                                       select e).ToList();
        }

        private void dtgEmpleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dtgEmpleados_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            EMPLEADO emp = (EMPLEADO)dtgEmpleados.SelectedRows[0].DataBoundItem;

            txtNombre.Text = emp.Nombre;
            txtApellido.Text = emp.Apellido;
            txtCuil.Text = emp.CUIL.ToString();
        }

        private void btnModificarEmpleado_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaText(txtNombre.Text) && VALIDADOR.ValidaText(txtApellido.Text) && VALIDADOR.ValidaCuil(txtCuil.Text))
            {
                EMPLEADO emp = (EMPLEADO)dtgEmpleados.SelectedRows[0].DataBoundItem;
                lblEstado.Text = EMPLEADO.ModificarEmpleado(emp.Legajo, txtNombre.Text, txtApellido.Text, Int64.Parse(txtCuil.Text));
                Enlazar();
                txtNombre.Clear();
                txtApellido.Clear();
                txtCuil.Clear();
            }
        }

        private void frmModificacionEmpleado_Load(object sender, EventArgs e)
        {

        }
    }
}

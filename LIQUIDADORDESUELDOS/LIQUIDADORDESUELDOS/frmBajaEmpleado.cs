﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace LIQUIDADORDESUELDOS
{
    public partial class frmBajaEmpleado : Form
    {
        public frmBajaEmpleado()
        {
            InitializeComponent();
            Enlazar();
        }

        void Enlazar()
        {
            dtgEmpleados.DataSource = null;
            dtgEmpleados.DataSource = (from EMPLEADO e in EMPLEADO.listarEmpleados()
                                       where e.Estado == 1
                                       select e).ToList();
        }

        private void btnModificarEmpleado_Click(object sender, EventArgs e)
        {
            EMPLEADO emp = (EMPLEADO)dtgEmpleados.SelectedRows[0].DataBoundItem;

            if(MessageBox.Show("Esta seguro de que quiere dar de baja al empleado " + emp.Nombre + "?", "Baja de empleados", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                lblEstado.Text = EMPLEADO.BajaEmpleado(emp.Legajo);
                Enlazar();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace LIQUIDADORDESUELDOS
{
    public partial class frmBajaConcepto : Form
    {
        public frmBajaConcepto()
        {
            InitializeComponent();
            Enlazar();
        }

        void Enlazar()
        {
            dtgConceptos.DataSource = null;
            dtgConceptos.DataSource = (from CONCEPTO c in CONCEPTO.listarConceptos()
                                       where c.Estado == 1
                                       select c).ToList();
        }

        private void btnBajaConcepto_Click(object sender, EventArgs e)
        {
            CONCEPTO cons = (CONCEPTO)dtgConceptos.SelectedRows[0].DataBoundItem;

            if (MessageBox.Show("Esta seguro de que quiere dar de baja el concepto " + cons.ID.ToString() + "?", "Baja de conceptos", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                lblResultado.Text = CONCEPTO.BajaConcepto(cons.ID);
                Enlazar();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace LIQUIDADORDESUELDOS
{
    public partial class frmInformes : Form
    {
        public frmInformes()
        {
            InitializeComponent();
            comboEmpleado.DataSource = null;
            comboEmpleado.DataSource = EMPLEADO.listarEmpleados();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboEmpleado.SelectedItem != null)
            {
                EMPLEADO emp = (EMPLEADO)comboEmpleado.SelectedItem;
                dtgRecibos.DataSource = null;
                dtgRecibos.DataSource = RECIBO.listarRecibos(emp.Legajo);
            }
        }

        private void dtgRecibos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dtgRecibos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            RECIBO res = (RECIBO)dtgRecibos.SelectedRows[0].DataBoundItem;
            dtgConceptosAplicados.DataSource = null;
            dtgConceptosAplicados.DataSource = res.Conceptos;
        }
    }
}

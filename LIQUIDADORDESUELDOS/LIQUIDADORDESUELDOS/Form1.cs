﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace LIQUIDADORDESUELDOS
{
    public partial class Form1 : Form
    {
        /* Para llevar a cabo el parcial opte por utilizar todos SP en la DB, ya que es verdad que simplifica las tareas en la BD
         y ademas me provee agilidad a la hora de modificar procesos de acceso a datos y tambien me deja el codigo mucho mas ordenado.
        Tal como establece el desarrollo en 3 capas, toda la capa de presentacion la dedique 100% a lo que respecta a la parte grafica de mi solucion y las validaciones de dato.
        En la parte de negocio puse todas las acciones y logicas que respectan a las clases propias de la solucion. 
        En la clase Acceso solo puse la parte que interactua con la DB.
        La capa de presentacion hace referencia al negocio, el negocio a Acceso y el acceso a ninguna. No hay referencias entre la capa
        de presentacion y la capa de datos.

        En este formulario solo tengo las llamadas a los demas formularios que componen la solucion, use mdi conteiner.
        */
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void altaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmAltaEmpleados frm = new frmAltaEmpleados();
            frm.MdiParent = this;
            frm.Show();
        }

        private void modificacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmModificacionEmpleado frm = new frmModificacionEmpleado();
            frm.MdiParent = this;
            frm.Show();
        }

        private void bajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBajaEmpleado frm = new frmBajaEmpleado();
            frm.MdiParent = this;
            frm.Show();
        }

        private void altaToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmAltaConcepto frm = new frmAltaConcepto();
            frm.MdiParent = this;
            frm.Show();
        }

        private void modificacionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmModificacionConcepto frm = new frmModificacionConcepto();
            frm.MdiParent = this;
            frm.Show();
        }

        private void bajaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmBajaConcepto frm = new frmBajaConcepto();
            frm.MdiParent = this;
            frm.Show();
        }

        private void altaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAltaRecibo frm = new frmAltaRecibo();
            frm.MdiParent = this;
            frm.Show();
        }

        private void informesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmInformes frm = new frmInformes();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}

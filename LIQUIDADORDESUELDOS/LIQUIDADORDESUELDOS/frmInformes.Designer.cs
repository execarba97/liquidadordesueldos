﻿namespace LIQUIDADORDESUELDOS
{
    partial class frmInformes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboEmpleado = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dtgRecibos = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtgConceptosAplicados = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dtgRecibos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgConceptosAplicados)).BeginInit();
            this.SuspendLayout();
            // 
            // comboEmpleado
            // 
            this.comboEmpleado.FormattingEnabled = true;
            this.comboEmpleado.Location = new System.Drawing.Point(94, 9);
            this.comboEmpleado.Name = "comboEmpleado";
            this.comboEmpleado.Size = new System.Drawing.Size(229, 24);
            this.comboEmpleado.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Empleado";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(329, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(84, 52);
            this.button1.TabIndex = 2;
            this.button1.Text = "Ver recibos";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(447, 10);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(84, 52);
            this.button2.TabIndex = 3;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // dtgRecibos
            // 
            this.dtgRecibos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgRecibos.Location = new System.Drawing.Point(15, 87);
            this.dtgRecibos.Name = "dtgRecibos";
            this.dtgRecibos.RowHeadersWidth = 51;
            this.dtgRecibos.RowTemplate.Height = 24;
            this.dtgRecibos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgRecibos.Size = new System.Drawing.Size(955, 262);
            this.dtgRecibos.TabIndex = 4;
            this.dtgRecibos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgRecibos_CellContentClick);
            this.dtgRecibos.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dtgRecibos_CellMouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Recibos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 363);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Conceptos aplicados al recibo";
            // 
            // dtgConceptosAplicados
            // 
            this.dtgConceptosAplicados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgConceptosAplicados.Location = new System.Drawing.Point(15, 383);
            this.dtgConceptosAplicados.Name = "dtgConceptosAplicados";
            this.dtgConceptosAplicados.RowHeadersWidth = 51;
            this.dtgConceptosAplicados.RowTemplate.Height = 24;
            this.dtgConceptosAplicados.Size = new System.Drawing.Size(955, 262);
            this.dtgConceptosAplicados.TabIndex = 7;
            // 
            // frmInformes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 723);
            this.Controls.Add(this.dtgConceptosAplicados);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtgRecibos);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboEmpleado);
            this.Name = "frmInformes";
            this.Text = "frmInformes";
            ((System.ComponentModel.ISupportInitialize)(this.dtgRecibos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgConceptosAplicados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboEmpleado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dtgRecibos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dtgConceptosAplicados;
    }
}
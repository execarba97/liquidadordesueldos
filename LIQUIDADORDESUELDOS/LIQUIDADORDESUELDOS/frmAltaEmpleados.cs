﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace LIQUIDADORDESUELDOS
{
    public partial class frmAltaEmpleados : Form
    {
        public frmAltaEmpleados()
        {
            InitializeComponent();
            Enlazar();
        }

        void Enlazar()
        {
            dtgEmpleados.DataSource = null;
            dtgEmpleados.DataSource = (from EMPLEADO e in EMPLEADO.listarEmpleados()
                                       where e.Estado == 1
                                       select e).ToList();
        }

        void Limpiar()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtCuil.Clear();
        }

        private void btnAgregarEmpleado_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaText(txtNombre.Text) && VALIDADOR.ValidaText(txtApellido.Text) && VALIDADOR.ValidaCuil(txtCuil.Text))
            {
                EMPLEADO.AltaEmpleado(txtNombre.Text, txtApellido.Text, Int64.Parse(txtCuil.Text));
                Enlazar();
                Limpiar();
            }
        }

        private void frmAltaEmpleados_Load(object sender, EventArgs e)
        {

        }

        private void dtgEmpleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

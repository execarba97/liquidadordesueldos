﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Acceso;

namespace Negocio
{
    public class EMPLEADO
    {
        public EMPLEADO(int leg, string nom, string ape, DateTime fech, int est, Int64 cui)
        {
            legajo = leg;
            nombre = nom;
            apellido = ape;
            fecha = fech;
            estado = est;
            cuil = cui;
        }

        private int legajo;

        public int Legajo
        {
            get { return legajo; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
        }

        private DateTime fecha;

        public DateTime Fecha
        {
            get { return fecha; }
        }

        private int estado;

        public int Estado
        {
            get { return estado; }
        }

        private Int64 cuil;

        public Int64 CUIL
        {
            get { return cuil; }
            set { cuil = value; }
        }

        public static List<EMPLEADO> listarEmpleados()
        {
            List<EMPLEADO> listaDeEmpleados = new List<EMPLEADO>();
            ACCESO acceso = new ACCESO();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("Leer_empleados");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                EMPLEADO emp = new EMPLEADO(int.Parse(registro["legajo"].ToString()), registro["nombre"].ToString(), registro["apellido"].ToString(), DateTime.Parse(registro["fecha"].ToString()), int.Parse(registro["estado"].ToString()),Int64.Parse(registro["cuil"].ToString()));
                listaDeEmpleados.Add(emp);
            }

            return listaDeEmpleados;
        }

        public static string AltaEmpleado(string nombre, string apellido, Int64 cuil)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("nombre", nombre));
            paratros.Add(acceso.CreaParametro("apellido", apellido));
            paratros.Add(acceso.CreaParametro("cuil", cuil));

            resultado = acceso.Escribir("Alta_empleados", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public static string ModificarEmpleado(int legajo, string nombre, string apellido, Int64 cuil)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("legajo", legajo));
            paratros.Add(acceso.CreaParametro("nombre", nombre));
            paratros.Add(acceso.CreaParametro("apellido", apellido));
            paratros.Add(acceso.CreaParametro("cuil", cuil));

            resultado = acceso.Escribir("Modifica_empleados", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public static string BajaEmpleado(int legajo)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("legajo", legajo));

            resultado = acceso.Escribir("Baja_empleados", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public override string ToString()
        {
            return nombre + " " + apellido;
        }
    }
}

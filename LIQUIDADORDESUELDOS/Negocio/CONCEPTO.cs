﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acceso;
using System.Data;
using System.Data.SqlClient;

namespace Negocio
{
    public class CONCEPTO
    {
        public CONCEPTO(int identificador, string desc, float porcent, int esta)
        {
            id = identificador;
            descripcion = desc;
            porcentaje = porcent;
            estado = esta;
        }

        private int id;

        public int ID
        {
            get { return id; }
        }

        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
        }

        private float porcentaje;

        public float Porcentaje
        {
            get { return porcentaje; }
        }

        private int estado;

        public int Estado
        {
            get { return estado; }
        }

        /*Use todas llamadas a el proyecto Acceso para poder llevar a cabo los metodos me componen a la clase.
         Mi metodo Escribir siempre se compone del SP al cual llamar y los marametros requeridos.*/
        public static string AltaConcepto(string descripcion, float porcentaje, enumConceptos conceptos)
        {
            if(conceptos == enumConceptos.suma)
            {
                porcentaje += 1;
            }

            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("descripcion", descripcion));
            paratros.Add(acceso.CreaParametro("porcentaje", porcentaje));

            resultado = acceso.Escribir("Alta_concepto", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public static string BajaConcepto(int id)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("id", id));

            resultado = acceso.Escribir("Baja_concepto", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public static string ModificaConcepto(int id, string descripcion, float porcentaje, enumConceptos conceptos)
        {
            // Los porcentajes de los conceptos se guardan 1< si son positivos.
            if (conceptos == enumConceptos.suma)
            {
                porcentaje += 1;
            }

            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("descripcion", descripcion));
            paratros.Add(acceso.CreaParametro("porcentaje", porcentaje));
            paratros.Add(acceso.CreaParametro("id", id));

            resultado = acceso.Escribir("Modifica_concepto", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public static List<CONCEPTO> listarConceptos()
        {
            List<CONCEPTO> listaDeConceptos = new List<CONCEPTO>();
            ACCESO acceso = new ACCESO();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("Leer_conceptos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                CONCEPTO cons = new CONCEPTO(int.Parse(registro["id"].ToString()), registro["descripcion"].ToString(), float.Parse(registro["porcentaje"].ToString()), int.Parse(registro["estado"].ToString()));
                listaDeConceptos.Add(cons);
            }

            return listaDeConceptos;
        }

        public static DataTable ObtenerConceptosAplicados()
        {
            ACCESO acceso = new ACCESO();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("Leer_conceptosAplicados");
            acceso.Cerrar();

            return tabla;
        }

        public static DataTable ObtenerConceptos()
        {   
            ACCESO acceso = new ACCESO();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("Leer_conceptos");
            acceso.Cerrar();

            return tabla;
        }

        public override string ToString()
        {
            return descripcion;
        }
    }
}

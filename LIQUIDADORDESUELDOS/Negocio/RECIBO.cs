﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Acceso;

namespace Negocio
{
    public class RECIBO
    {
        /*Dentro de esta clase opte por tener un atributo que almacena los conceptos que conforman el recibo. De esta forma
         es mas simple obtener el detalle del recibo en la parte de informes.*/
        public RECIBO(int identificador, int idEmp, int me, int an, float neto, float bruto, List<CONCEPTO> cons = null)
        {
            id = identificador;
            idEmpleado = idEmp;
            mes = me;
            ano = an;
            sueldoNeto = neto;
            sueldoBruto = bruto;
            Conceptos = cons;
        }

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private int idEmpleado;

        public int IDEmpleado
        {
            get { return idEmpleado; }
            set { idEmpleado = value; }
        }

        private int mes;

        public int MES
        {
            get { return mes; }
            set { mes = value; }
        }

        private int ano;

        public int ANO
        {
            get { return ano; }
            set { ano = value; }
        }

        private List<CONCEPTO> conceptos;

        public List<CONCEPTO> Conceptos
        {
            get { return conceptos; }
            set { conceptos = value; }
        }

        private float sueldoNeto;

        public float SueldoNeto
        {
            get { return sueldoNeto; }
            set { sueldoNeto = value; }
        }

        private float sueldoBruto;

        public float SueldoBruto
        {
            get { return sueldoBruto; }
            set { sueldoBruto = value; }
        }


        public static List<RECIBO> listarRecibos()
        {
            List<RECIBO> listaDeRecibos = new List<RECIBO>();
            ACCESO acceso = new ACCESO();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("Leer_recibos");
            acceso.Cerrar();

            DataTable ConceptosAplicados = CONCEPTO.ObtenerConceptosAplicados();
            
            foreach (DataRow registro in tabla.Rows)
            {
                List<CONCEPTO> cons = new List<CONCEPTO>();

                // Aca es donde busco los conceptos aplicados al recibo, itereando en los conceptos aplicados.
                foreach (DataRow c in ConceptosAplicados.Rows)
                {
                    if(c["id_recibo"].ToString() == registro["id"].ToString())
                    {
                        CONCEPTO concepto = new CONCEPTO(int.Parse(c["id_concepto"].ToString()), c["descripcion"].ToString(), float.Parse(c["porcentaje"].ToString()), int.Parse(c["estado"].ToString()));
                        cons.Add(concepto);
                    }
                }
                
                RECIBO res = new RECIBO(int.Parse(registro["id"].ToString()), int.Parse(registro["id_empleado"].ToString()), int.Parse(registro["mes"].ToString()), int.Parse(registro["año"].ToString()), float.Parse(registro["sueldo_neto"].ToString()), float.Parse(registro["sueldo_bruto"].ToString()), cons);
                listaDeRecibos.Add(res);
            }

            return listaDeRecibos;
        }

        public static string GenerarRecibo(int idEmpleado, int mes, int ano, float sueldoBruto, List<CONCEPTO> conceptos = null)
        {
            string idConceptos = "";
            float sueldoNeto = sueldoBruto; 
            
            if (conceptos != null)
            {
                int flag = 0;
                
                foreach (CONCEPTO c in conceptos)
                {
                    // En este paso yo delimito por , los id de los conceptos para luego pasarlos al SP e insertar todo
                    // en las tablas correspondientes.
                    if (flag == 0)
                    {
                        idConceptos = c.ID.ToString();
                        flag = 1;
                    }
                    else
                    {
                        idConceptos += "," + c.ID.ToString();
                    }

                    // Aca hago el calculo del sueldo neto
                    if(c.Porcentaje > 1)
                    {
                        sueldoNeto += (sueldoBruto * c.Porcentaje);
                    }
                    else
                    {
                        sueldoNeto -= (sueldoBruto * c.Porcentaje);
                    }
                }
            }

            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("Parametros", idConceptos));
            paratros.Add(acceso.CreaParametro("id_empleado", idEmpleado));
            paratros.Add(acceso.CreaParametro("mes", mes));
            paratros.Add(acceso.CreaParametro("año", ano));
            paratros.Add(acceso.CreaParametro("sueldo_neto", sueldoNeto));
            paratros.Add(acceso.CreaParametro("sueldo_bruto", sueldoBruto));

            resultado = acceso.Escribir("Alta_recibo", paratros);
            acceso.Cerrar();

            return resultado;
        }

        // Aca listo los recibos de un determinado empleado.
        public static List<RECIBO> listarRecibos(int id_empleado)
        {
            List<RECIBO> recibosTotales = listarRecibos();

            List<RECIBO> recibosEmpleado = (from RECIBO r in recibosTotales
                                            where r.idEmpleado == id_empleado
                                            select r).ToList();

            return recibosEmpleado;
        }
    }
}
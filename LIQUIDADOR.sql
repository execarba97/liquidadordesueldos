USE [master]
GO
/****** Object:  Database [LIQUIDADORSUELDOS]    Script Date: 9/30/2020 11:55:40 AM ******/
CREATE DATABASE [LIQUIDADORSUELDOS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LIQUIDADORSUELDOS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\LIQUIDADORSUELDOS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LIQUIDADORSUELDOS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\LIQUIDADORSUELDOS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LIQUIDADORSUELDOS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET ARITHABORT OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET  MULTI_USER 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET QUERY_STORE = OFF
GO
USE [LIQUIDADORSUELDOS]
GO
/****** Object:  Table [dbo].[CONCEPTOS]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONCEPTOS](
	[id] [int] NOT NULL,
	[descripcion] [varchar](50) NULL,
	[porcentaje] [float] NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_CONCEPTOS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CONCEPTOS_APLICADOS]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONCEPTOS_APLICADOS](
	[id_empleado] [int] NOT NULL,
	[id_recibo] [int] NOT NULL,
	[id_concepto] [int] NOT NULL,
 CONSTRAINT [PK_CONCEPTOS_APLICADOS] PRIMARY KEY CLUSTERED 
(
	[id_empleado] ASC,
	[id_recibo] ASC,
	[id_concepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EMPLEADOS]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EMPLEADOS](
	[legajo] [int] NOT NULL,
	[nombre] [varchar](30) NULL,
	[apellido] [varchar](30) NULL,
	[fecha] [date] NULL,
	[estado] [int] NULL,
	[cuil] [numeric](18, 0) NULL,
 CONSTRAINT [PK_EMPLEADOS] PRIMARY KEY CLUSTERED 
(
	[legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RECIBOS]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECIBOS](
	[id_empleado] [int] NOT NULL,
	[id] [int] NOT NULL,
	[mes] [int] NULL,
	[año] [int] NULL,
	[sueldo_neto] [float] NULL,
	[sueldo_bruto] [float] NULL,
 CONSTRAINT [PK_RECIBOS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Alta_concepto]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Alta_concepto]
@descripcion varchar(30),
@porcentaje float
as
begin
	declare @id int, @estado int
	set @estado = 1
	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.CONCEPTOS)
	
	INSERT INTO [dbo].[CONCEPTOS] ([id],[descripcion],[porcentaje],[estado]) 
	VALUES (@id, @descripcion, @porcentaje, @estado)
end
GO
/****** Object:  StoredProcedure [dbo].[Alta_empleados]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Alta_empleados]
@nombre varchar(30),
@apellido varchar(30),
@cuil numeric
as
begin
	declare @legajo int
	set @legajo = (SELECT ISNULL(MAX(legajo), 0)+1 FROM dbo.EMPLEADOS)
	
	INSERT INTO [dbo].[EMPLEADOS] ([legajo],[nombre],[apellido],[cuil],[fecha],[estado]) 
	VALUES (@legajo, @nombre, @apellido, @cuil, GETDATE(), 1)
end
GO
/****** Object:  StoredProcedure [dbo].[Alta_recibo]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Alta_recibo]
@Parametros varchar(1000),
@id_empleado int,
@mes int,
@año int,
@sueldo_neto float,
@sueldo_bruto float

AS

DECLARE @Posicion int
DECLARE @id int
DECLARE @Parametro varchar(1000)

set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.RECIBOS)
INSERT INTO [dbo].[RECIBOS] ([id_empleado],[id],[mes],[año],[sueldo_neto],[sueldo_bruto])
VALUES (@id_empleado, @id, @mes, @año, @sueldo_neto, @sueldo_bruto)

SET @Parametros = @Parametros + ','
WHILE patindex('%,%' , @Parametros) <> 0

BEGIN
	SELECT @Posicion =  patindex('%,%' , @Parametros)
	SELECT @Parametro = left(@Parametros, @Posicion - 1)
	INSERT INTO [dbo].CONCEPTOS_APLICADOS (id_empleado,id_recibo,id_concepto)
	values (@id_empleado, @id, @Parametro)
	SELECT @Parametros = stuff(@Parametros, 1, @Posicion, '')
END
GO
/****** Object:  StoredProcedure [dbo].[Baja_concepto]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Baja_concepto]
@id int
as
begin
	UPDATE [dbo].[CONCEPTOS] SET [estado] = 0
	WHERE id = @id
end
GO
/****** Object:  StoredProcedure [dbo].[Baja_empleados]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Baja_empleados]
@legajo int
as
begin
	UPDATE [dbo].[EMPLEADOS] SET [estado] = 0 where legajo = @legajo
end
GO
/****** Object:  StoredProcedure [dbo].[Leer_conceptos]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[Leer_conceptos]
as
begin
	SELECT [id],[descripcion],[porcentaje],[estado]
	FROM [LIQUIDADORSUELDOS].[dbo].[CONCEPTOS]
end

GO
/****** Object:  StoredProcedure [dbo].[Leer_conceptosAplicados]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Leer_conceptosAplicados]
as
begin
	select cons.id as 'id_concepto', res.id as 'id_recibo', cons.descripcion, cons.porcentaje, cons.estado
	FROM [CONCEPTOS] as cons
	inner join [CONCEPTOS_APLICADOS] as co
	on cons.id = co.id_concepto
	inner join [RECIBOS] as res
	on res.id = co.id_recibo
end
GO
/****** Object:  StoredProcedure [dbo].[Leer_empleados]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Leer_empleados]
as
begin
	SELECT [legajo],[nombre],[apellido],[fecha],[estado],[cuil]
	FROM [LIQUIDADORSUELDOS].[dbo].[EMPLEADOS]
end

GO
/****** Object:  StoredProcedure [dbo].[Leer_recibos]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[Leer_recibos]
as
begin
	SELECT [id_empleado],[id],[mes],[año],[sueldo_neto],[sueldo_bruto]
	FROM [LIQUIDADORSUELDOS].[dbo].[RECIBOS]
end

GO
/****** Object:  StoredProcedure [dbo].[Modifica_concepto]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Modifica_concepto]
@id int,
@descripcion varchar(30),
@porcentaje float
as
begin
	UPDATE [dbo].[CONCEPTOS] SET [estado] = 0 WHERE id = @id

	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.CONCEPTOS)
	
	INSERT INTO [dbo].[CONCEPTOS] ([id],[descripcion],[porcentaje],[estado]) 
	VALUES (@id, @descripcion, @porcentaje, 1)
end
GO
/****** Object:  StoredProcedure [dbo].[Modifica_empleados]    Script Date: 9/30/2020 11:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Modifica_empleados]
@legajo int,
@nombre varchar(30),
@apellido varchar(30),
@cuil numeric(18,0)
as
begin
	UPDATE [dbo].[EMPLEADOS] 
	SET [nombre] = @nombre, [apellido] = @apellido, [cuil] = @cuil 
	where legajo = @legajo
end
GO
USE [master]
GO
ALTER DATABASE [LIQUIDADORSUELDOS] SET  READ_WRITE 
GO
